﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Shop.Dto;
using Shop.Enums;
using Shop.Models;

namespace Shop
{
	public class ProductService
	{
		private readonly ShopContext _dbContext;

		private IList<Product> Products => _dbContext.Products.Include(p => p.Category).ToList();

		public ProductService(ShopContext dbContext)
		{
			_dbContext = dbContext;
		}

		/// <summary>
		/// Сортирует товары по цене и возвращает отсортированный список
		/// </summary>
		/// <param name="sortOrder">Порядок сортировки</param>
		public IEnumerable<Product> SortByPrice(SortOrder sortOrder)
		{
			
			if (sortOrder == 0)
			{
				return		from prod in Products
							 orderby prod.Price
							 select prod;
			}
			else
			{
				return		 from prod in Products
							 orderby prod.Price descending
							 select prod;
			}
			
		}

		/// <summary>
		/// Возвращает товары, название которых начинается на <see cref="name"/>
		/// </summary>
		/// <param name="name">Фильтр - строка, с которой начинается название товара</param>
		public IEnumerable<Product> FilterByNameStart(string name)
		{
			return from prod in Products
				   where string.CompareOrdinal(prod.Name, 0, name, 0, name.Length) == 0
				   select prod;
		}

		/// <summary>
		/// Группирует товары по производителю
		/// </summary>
		public IDictionary<string, List<Product>> GroupByVendor()
		{
			var groupedList = Products.GroupBy(prod=>prod.Vendor);
			var dictList = groupedList.ToDictionary(groupedList => groupedList.Key,	groupedList=>groupedList.ToList());
			return dictList;
        }

		/// <summary>
		/// Возвращает список самых дорогих товаров (самые дорогие - товары с наибольшей ценой среди всех товаров)
		/// </summary>
		public IEnumerable<Product> GetTheMostExpensiveProducts()
		{
			var maxPrice = Products.Max(product => product.Price);
			return from prod in Products
				   where prod.Price == maxPrice
				   select prod;
		}

		/// <summary>
		/// Возвращает список самых дешевых товаров (самые дешевые - товары с наименьшей ценой среди всех товаров)
		/// </summary>
		public IEnumerable<Product> GetTheCheapestProducts()
		{
			var minPrice = Products.Min(product => product.Price);
			return from prod in Products
				   where prod.Price == minPrice
				   select prod;
		}

		/// <summary>
		/// Возвращает среднюю цену среди всех товаров
		/// </summary>
		public decimal GetAverageProductPrice()
		{
			return Products.Average(product => product.Price);
		}

		/// <summary>
		/// Возвращает среднюю цену товаров в указанной категории
		/// </summary>
		/// <param name="categoryId">Идентификатор категории</param>
		public decimal GetAverageProductPriceInCategory(int categoryId)
		{
			var categoryProdList = from prod in Products
								where prod.CategoryId == categoryId
								select prod;
			return categoryProdList.Average(product => product.Price);
		}

		/// <summary>
		/// Возвращает список продуктов с актуальной ценой (после применения скидки)
		/// </summary>
		public IDictionary<Product, decimal> GetProductsWithActualPrice()
		{
			return Products.ToDictionary(priceList => priceList, priceList => priceList.Price - (priceList.Price * priceList.Discount / 100));
		}

		/// <summary>
		/// Возвращает список продуктов, сгруппированный по производителю, а внутри - по названию категории.
		/// Продукты внутри последней группы отсортированы в порядке убывания цены
		/// </summary>
		public IList<VendorProductsDto> GetGroupedByVendorAndCategoryProducts()
		{
			
			var grouped = from prod in Products
						  group prod by prod.Vendor; 

			return (from p in grouped
				   select new VendorProductsDto
				   {
					   Vendor = p.Key,
					   CategoryProducts = p.GroupBy(p => p.Category.Name)
														.ToDictionary(p => p.Key,
																	  p => p.OrderByDescending(p => p.Price).ToList())
				   }).ToList();

        }

        /// <summary>
        /// Обновляет скидку на товары, которые остались на складе в количестве 1 шт,
        /// и возвращает список обновленных товаров
        /// </summary>
        /// <param name="newDiscount">Новый процент скидки</param>
        public IEnumerable<Product> UpdateDiscountIfUnitsInStockEquals1AndGetUpdatedProducts(int newDiscount)
		{
			return from prod in Products
				   where prod.UnitsInStock == 1
				   select new Product
				   {
					   Name = prod.Name,
					   Category = prod.Category,
					   CategoryId = prod.CategoryId,
					   Price = prod.Price,
					   Discount = newDiscount,
					   Id = prod.Id,
					   Vendor = prod.Vendor,
					   UnitsInStock = prod.UnitsInStock
				   };
		}
	}
}
